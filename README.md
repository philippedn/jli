# JLI

## Introduction

The goal of the `jli` script is to create a minimal JVM image to run a Java application. It is further able to create a detailed relationship diagram of an application's dependencies and a package with `jpackage`.

## Intend

To create a `jlink` image for an application, there are three steps to be performed:

1. Determine the java modules to add to the jlink image
2. Execute `jlink`
3. Organize jars for the module- and class path

`jli` automates these steps for all different use cases, i.e. non-modularized, partially modularized, and fully modularized applications.

In the sub sections below the different steps are described in more detail.

### Determine the java modules to add to the jlink image

Given a directory `jars` with all the libraries the application depends on, the first step can be performed as follows:

```bash
jdeps --multi-release 21 --add-modules ALL-DEFAULT --print-module-deps --ignore-missing-deps jars/*
```

`jdeps` will automatically put modularized jars on the module path and non-modularized jars on the class path. This is an issue if there are modules that depend on automatic modules, because these automatic modules must be on the module path in order to resolve the module dependency tree. As a workaround one can additionally put all jars on the module path, but this will produce warnings because of split packages, i.e. for the packages of the non-modularized jars which now are also modules. One can filter out these warnings, e.g. with `grep`:

```bash
jdeps --multi-release 21 --add-modules ALL-DEFAULT --module-path jars --print-module-deps --ignore-missing-deps jars/* | grep -v '^Warning: split package'
```

However, the command will not produce the desired output in the following cases:

- If there are modularized jars with static module dependencies and these dependencies are loaded via reflection, `jdeps` will not report these dependencies.
- If there are modularized jars with non-static module dependencies that are not present, the command will fail. For example, consider an application that depends on `hibernate.core`, which in turn depends on `jakarta.transaction`, which relies on `jakarta.cdi` and `jakarta.interceptor`. In the POM file of `jakarta.transaction`, these two dependencies are defined as provided, meaning they are not included in the final application. This is not an issue when working with `jakarta.transaction` on the classpath. However, since it is a modularized JAR, `jdeps` treats it as a module and fails due to the missing dependencies.

Unfortunately there is no `jdeps` command that will work in all situations. `jli` offers a solution for this.

### Execute `jlink`

The second step involves executing `jlink`, typically with the following options:

```bash
jlink --add-modules $(jdeps ...) --compress "zip-6" --strip-debug --no-header-files --no-man-pages --output output_dir
```

### Organize jars for the module- and class path

The third step involves organizing jars, i.e. to put them on the class path and/or module path. There are several methods:

1. Put all jars on the class path.
2. Put all jars the module path.
3. Put modularized jars and their automatic module dependencies on the module path; keep all other jars on the class path.
4. Put module dependencies of the main module, together with their transitive module dependencies and their direct automatic module dependencies, on the module path. Keep all the other jars on the class path. This is how Eclipse and the Maven Surefire plugin run an application.

`jli` can organize jars according to methods 3 and 4 (organization according to methods 1 or 2 would be no-ops).

`jli` can also put modules in the jlink image instead of putting them on the module path.

## Usage

### Help

```bash
jli -h
```

```
Usage: jli [-hvwx] [-g ANALYSIS_FILE] [-J TARGET_JVM] [-l JARS_DIR] [-D OUTPUT_DIR] [-d [-j] [-n]] (([-i] [-P [JP_OPT_FILE] [-M MAIN_MOD/MAIN_CLASS] [-O]]) [-a EXTRA_MODS] [-I JL_OPT_FILE]) [-o [ORG_ROOTS] [-p] [-q]] [-s] [-e LOG_LEVEL] [-u LOG_TIME] [-t THREADS] [-y JA_STRAT]
        -a   Comma separated list of extra modules to add to the image or package. Modules prefixed with '-' will be excluded (not transitively). This option is also valid when dot file creation is requested.
        -d   Create dot file
        -D   Output directory. Defaults to the same directory JARS_DIR resides in, or the current directory if JARS_DIR is not specified.
        -e   Specify log level. Possible values: TRACE, DEBUG, INFO, WARN, ERROR, OFF.
        -g   Load analysis from file
        -h   Print usage and exit
        -i   Create jlink image
        -I   Provide a file JL_OPT_FILE containing options to be passed to jlink instead of the defaults (--compress zip-6 --strip-debug --no-header-files --no-man-pages). Prepend with + to add to the defaults.
        -j   Add Java modules to dot file
        -J   Use another target JVM. Default is the JVM in which the java executable is found (in $JAVA_HOME or $PATH).
        -l   Directory with jar files to analyze
        -M   Main module and class for package creation. Use automatic module name in case it's a non-modularized jar.
        -n   Add not available dependencies to dot file
        -o   Organize jars for the module- and class path. Optionally provide ORG_ROOTS as a comma separated list of modules to put on the module path (transitively), instead of all modules.
        -O   Organize jars for the module- and class path, but don't put the actual result in the output directory. Can be combined with options related to the -o option.
        -p   Put modules in the jlink image instead of in the module path directory
        -P   Create a package with jpackage. Optionally specify a file JP_OPT_FILE containing options to be passed to jpackage instead of the defaults. Prepend with + to add to the defaults.
        -q   Move jars from JARS_DIR instead of copying them
        -s   Save analysis
        -t   Number of threads to use for parallelized tasks. Default is number of CPUs.
        -u   Specify time to show in logs. Possible values: CURRENT, DIFF, START.
        -v   Print version and exit
        -w   Keep work directory
        -x   Enable debug mode
        -y   Specify jar analysis strategy. Possible values: JAR, JLM.
```

### Needed utilities

`jli` depends on several command line utilities which need to be installed in order for it to run. Below is a table listing these utilities and which package they are part of on Debian, MSYS2, and MacOS systems.

| Utility        | Debian apt       | [MSYS2 pacman](#windows) | [MacOS brew](#macos) |
| -------------- | ---------------- | ------------------------ | -------------------- |
| `java`[^1]     | `openjdk-xx-jdk` | /                        | `java`               |
| `jar`[^1]      | `openjdk-xx-jdk` | /                        | `java`               |
| `jdeps`[^1]    | `openjdk-xx-jdk` | /                        | `java`               |
| `jlink`[^1]    | `openjdk-xx-jdk` | /                        | `java`               |
| `jmod`[^1]     | `openjdk-xx-jdk` | /                        | `java`               |
| `jpackage`[^1] | `openjdk-xx-jdk` | /                        | `java`               |
| `pgrep`        | `procps`         | `procps-ng`              | n/a                  |
| `pkill`        | `procps`         | `procps-ng`              | n/a                  |
| `flock`        | `util-linux`     | `util-linux`             | `flock`              |
| `column`       | `bsdextrautils`  | `util-linux`             | n/a                  |
| `nproc`        | `coreutils`      | `coreutils`              | `coreutils`          |
| `stdbuf`       | `coreutils`      | `coreutils`              | `coreutils`          |
| `unzip`        | `unzip`          | `unzip`                  | n/a                  |
| `shunit2`[^2]  | `shunit2`        | /                        | `shunit2`            |

[^1]: The utility will first be searched for in `$JAVA_HOME/bin`. If not present, it will be searched for in `$PATH`, where `openjdk-xx-jdk` puts it.
[^2]: Only needed to run the tests

### Git repo

In the Git repo there are a couple of binary files used by the integration tests that are managed by [git-lfs](https://git-lfs.com/). Before cloning the repo, install the `git-lfs` package and then issue the command `git lfs install`. If you're not planning to execute the integration tests, this is not necessary; the binary files are not needed by the `jli` script.

To download the binaries managed by git lfs after you've already cloned the repo, install git lfs as described in the previous paragraph and then issue the following commands:

```bash
git lfs fetch
git lfs checkout
```

## Creating JARS_DIR

All jars to take into account are expected to be present in the `JARS_DIR` directory (`-l` option). When using Maven, such a directory can be created as follows:

```
<plugin>
  <artifactId>maven-clean-plugin</artifactId>
  <executions>
    <execution>
      <id>delete-jars-directory</id>
      <phase>prepare-package</phase>
      <goals>
        <goal>clean</goal>
      </goals>
      <configuration>
        <excludeDefaultDirectories>true</excludeDefaultDirectories>
        <filesets>
          <fileset>
            <directory>${project.build.directory}/jars</directory>
          </fileset>
        </filesets>
      </configuration>
    </execution>
  </executions>
</plugin>
<plugin>
  <artifactId>maven-dependency-plugin</artifactId>
  <executions>
    <execution>
      <id>create-jars-directory</id>
      <phase>prepare-package</phase>
      <goals>
        <goal>copy-dependencies</goal>
      </goals>
      <configuration>
        <includeScope>runtime</includeScope>
        <outputDirectory>${project.build.directory}/jars</outputDirectory>
        <silent>true</silent>
      </configuration>
    </execution>
  </executions>
</plugin>
<plugin>
  <artifactId>maven-jar-plugin</artifactId>
  <configuration>
    <outputDirectory>${project.build.directory}/jars</outputDirectory>
  </configuration>
</plugin>
```

## Output

The script will create the following filesystem entries in the same directory `JARS_DIR` resides:

1. `jli/mp` - Directory containing modularized jars and their automatic module dependencies (`-o` option)
2. `jli/cp` - Directory containing non-modularized jars that are not automatic module dependencies (`-o` option)
3. `jli/jvm` - The jlink image (`-i` option)
4. `jli/graph.dot` - Graphviz dot file (`-d` option)
5. `jli/<jpackage artifacts>` - Artifacts created by `jpackage` (`-P` option)

When there are root modules specified to the `-o` options (`ORG_ROOTS`), only direct dependent modules of `ORG_ROOTS` and their transitive dependencies, together with direct automatic module dependencies, will be put in the `mp` directory or linked into the image (`-p` option); cf. [intend](#organize-jars-for-the-module--and-class-path). Note that when using the `-p` option, it's not allowed to have automatic module dependencies.

Modules linked in the jlink image via the `-a` or `-p` options are not put into `jli/mp` or `jli/cp`.

## Graphviz dot file

`jli` can generate a detailed relationship diagram of an application's dependencies in the Graphiz `dot` format. This is useful for analysis of the dependencies of an application. Compared to `jdeps`' `dot` file creation, `jli` shows more details and allows to tweak the output.

To create the dot file, use the `-d` option. By default, Java modules and not available modules are not included. To include Java modules, add the `-j` option. To add not available modules, add the `-n` option. An `svg` file can be created from a `dot` file with the `dot` utility, which is part of the `graphviz` package.

An example for a modularized JavaFX application, with Java modules:

```bash
cd demos/jfxm
mvn package -Dexec.skip # to create the jars directory
cd target
jli -l jars -d -j
dot -Tsvg -o jfxm.svg jli/graph.dot
```

![dot file example jfxm](images/jfxm.svg)

An example for a non-modularized Spring Boot web application, without Java modules, with not available modules. `spring.jcl` has been excluded to improve readability.

```bash
cd demos/sbnm
mvn package -Dexec.skip # to create the jars directory
cd target
jli -l jars -d -n -a "-spring.jcl"
dot -Tsvg -o sbnm.svg jli/graph.dot
```

![dot file example sbnm](images/sbnm.svg)

Legend:

- Modularized jars not declared open are dark green
- Modularized jars declared open are light green
- Non-modularized jars having an `Automatic-Module-Name` attribute in their manifest are dark blue
- Non-modularized jars not having an `Automatic-Module-Name` attribute in their manifest are light blue
- Not available modules are red
- Java modules are brown
- Solid arrows denote regular dependencies
- Dashed arrows denote static dependencies
- Blue arrows denote transitive dependencies

## Creating a package with jpackage

To create a package with `jpackage`, specify the `-P` option.

Options passed to `jpackage` will be automatically determined based on the app's jars and if organization was requested or not. If no `-M` option was specified, the main jar and class will be chosen from the jars having a `Main-Class` attribute in their manifest; the jar with the most (recursive) dependencies will be selected.

To override the automatic determination of options passed to `jpackage`, provide the `JP_OPT_FILE` argument to `-P`, which is a file containing the options. To also make use of the automatic determination while providing custom options, prepend `JP_OPT_FILE` with a `+` sign.

## Jar analysis strategy

To determine module names and types, by default the command `java --list-modules --module-path $JARS_DIR` is used, which is very fast, but might fail if there are split packages. Alternatively one can request `jli` to determine module names and types by using the command `jar --describe-module --file $JARS_DIR/$JAR` for every jar file, which is slower, but will not fail in case of split packages; this is accomplished by adding the `-y` option with the value `JAR`.

## Windows

To use `jli` on Windows, either run it on [WSL](https://learn.microsoft.com/en-us/windows/wsl/about) or [MSYS2](https://www.msys2.org/).

## MacOS

On MacOS, first install [Homebrew](https://brew.sh/), which is to be used to install Bash, the utilities mentioned in the section [needed utitilities](#needed-utilities), and the GNU versions of common unix commands like `sed`, `awk`, `grep`, etc.

```bash
brew install bash java flock coreutils grep gnu-sed gawk findutils shunit2
```

And then set the `JAVA_HOME` and `PATH` variables:

```bash
export JAVA_HOME="/usr/local/opt/java/libexec/openjdk.jdk/Contents/Home"
export PATH="/usr/local/opt/findutils/libexec/gnubin:/usr/local/opt/gawk/libexec/gnubin:/usr/local/opt/gnu-sed/libexec/gnubin:/usr/local/opt/grep/libexec/gnubin:/usr/local/opt/coreutils/libexec/gnubin:$PATH"
```

## Examples

Below is assumed that the main jar has a manifest containing the `Main-Class` attribute with a value of `package.Main`. This mathers for the package creation.

### Non-modularized app

#### Image

```bash
jli -l jars -i # create image
jli/jvm/bin/java -cp 'jars/*' package.Main # run
```

#### Package

```bash
jli -l jars -P +<(echo --type app-image) # create package
jli/Main/bin/Main # run
```

### Partially modularized app via buttom-up approach

#### Image

```bash
jli -l jars -i -o # create image
jli/jvm/bin/java -p jli/mp -cp 'jli/cp/*' package.Main # run
```

#### Package

```bash
jli -l jars -P +<(echo --type app-image) -O # create package
jli/Main/bin/Main # run
```

### Partially modularized app via top-down approach

#### Image

```bash
jli -l jars -i -o main.module # create image
jli/jvm/bin/java -p jli/mp -cp 'jli/cp/*' -m main.module/package.Main # run
```

#### Package

```bash
jli -l jars -P +<(echo --type app-image) -O main.module # create package
jli/Main/bin/Main # run
```

### Fully modularized app

#### Image

```bash
jli -l jars -i -o -p # create image
jli/jvm/bin/java -m main.module/package.Main # run
```

#### Package

```bash
jli -l jars -P +<(echo --type app-image) -O -p # create package
jli/Main/bin/Main # run
```

## Demo applications

In the `demos` directory, there are demo projects available on how to use `jli` with Maven for different use cases:

- Non-modularized Spring Boot application
- Modularized Spring Boot application
- Non-modularized JavaFX application
- Modularized JavaFX application

These projects are configured to create a package with `jpackage`, for Linux, Windows, or MacOS, depending on the system you're running on.

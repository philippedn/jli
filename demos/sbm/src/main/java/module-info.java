module be.pdn.jli.demo.sbm {
  requires spring.boot;
  requires spring.boot.autoconfigure;
  requires spring.context;
  requires spring.web;
  requires spring.core; // Forces Eclipse and jli to add spring.core as module
                        // We don't want spring.core in the default module to omit
                        // having to open be.pdn.jli.demo.sbm to ALL-UNNAMED on the
                        // command line (spring.core needs reflective access to it's
                        // callers)

  opens be.pdn.jli.demo.sbm to spring.context, spring.core, spring.web;

  requires spring.beans; // Proxies created at runtime need to be able to read spring.beans

  exports be.pdn.jli.demo.sbm to spring.beans; // And these proxies are accessed by spring.beans itself.
}
